<?php
namespace App\DataFixtures;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\DataFixtures\CityFixtures;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class UserFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
    $roles = [0 => 'ROLE_ADMIN',1 => 'ROLE_OWNER',2 => 'ROLE_INSTRUCTOR',3 => 'ROLE_EMPLOYEE', 4 => 'ROLE_CLIENT'];
        for ($i = 0; $i < 50; $i++) {
            $user = new User();
            $user->setFullName('Name '.$i);
            $rolesUser = array();
            $rolesUser[0] = $roles[mt_rand(0,4)];
            $rolesUser[1] = $roles[mt_rand(0,4)];
            $user->setRoles($rolesUser);
            $user->setUsername('Username'.$i);
            $user->setPhone('Phone '.$i);
            $user->setEmail('Email '.$i);
            $user->setPlainPassword('Password'.$i);
            $user->setAddressLocation('AddressLocation '.$i);
            $user->setCity($this->getReference('city '.mt_rand(0,20)));
            $manager->persist($user);
        }
        $manager->flush();
    }
    
          public function getDependencies()
    {
        return array(
        CityFixtures::class,
        );
    }
}