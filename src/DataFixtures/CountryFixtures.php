<?php
namespace App\DataFixtures;
use App\Entity\Country;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CountryFixtures extends Fixture
{
    

    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i <= 5; $i++) {
            $country = new Country();
            $country->setName('Country '.$i);
            $country->setEnabled(TRUE);
            $this->addReference('country '.$i, $country);
            $manager->persist($country);
        }
        $manager->flush();

    }
}
