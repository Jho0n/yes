<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\User;
class CustomUser extends Controller
{
    public function __invoke(User $data,Request $request)
    {
       if (Request::METHOD_POST == $request->getMethod()){
           
           if($this->getDoctrine()->getRepository('App:User')->findOneByUsername($data->getUsername())){
                $response = array();
                $response['title'] = "An error occurred";
                $response['detail'] = "Username not available";
                return new JsonResponse($response,403);
       }
       
           if ($this->getDoctrine()->getRepository('App:User')->findOneByEmail($data->getEmail())){
               $response = array();
               $response['title'] = "An error occurred";
               $response['detail'] = "Email not available";
               return new JsonResponse($response,403);
       }
        return $data;
           
       }
       
       
       if (Request::METHOD_PUT == $request->getMethod()){
           
             $dabUsername = $this->getDoctrine()->getRepository('App:User')->findOneByUsername($data->getUsername());
             $dabEmail = $this->getDoctrine()->getRepository('App:User')->findOneByEmail($data->getEmail());
             
           if($dabUsername && ($dabUsername->getId() != $data->getId())){
                $response = array();
                $response['title'] = "An error occurred";
                $response['detail'] = "Username not available";
                return new JsonResponse($response,403);
       }
       
       
           if ($dabEmail && ($dabEmail->getId() != $data->getId())){
               $response = array();
               $response['title'] = "An error occurred";
               $response['detail'] = "Email not available";
               return new JsonResponse($response,403);
       }
        return $data;
           
       } 
           if (Request::METHOD_DELETE == $request->getMethod()){
           $em = $this->getDoctrine()->getEntityManager();
           $user = $em->getRepository("App:User")->find($data->getId());
           $response = array();
          try {
              $em->remove($user);
              $em->flush();
              $response['title'] = "Successfull";
              $response['detail'] = "User deleted";
              $status = 200;
          } catch (Exception $ex){
             $response['title'] = "An error occurred";
             $response['detail'] = "User not deleted";
             $status = 403;
          }
            return new JsonResponse($response,$status);
       }
       
}
}
