<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Dependence;
class CustomDependence extends Controller
{
    public function __invoke(Dependence $data,Request $request)
    {        
       if (Request::METHOD_DELETE == $request->getMethod()){
           $em = $this->getDoctrine()->getEntityManager();
           $discipline = $em->getRepository("App:Dependence")->find($data->getId());
           $response = array();
          try {
              $em->remove($discipline);
              $em->flush();
              $response['title'] = "Successfull";
              $response['detail'] = "Dependence deleted";
              $status = 200;
          } catch (Exception $ex){
             $response['title'] = "An error occurred";
             $response['detail'] = "Dependence not deleted";
             $status = 403;
          }
               return new JsonResponse($response,$status);
       }
}
}
