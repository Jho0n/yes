<?php
namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\CustomDiscipline;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;


/**
 * Discipline
 * @ApiResource(
 *     attributes={"pagination_enabled"=true,"pagination_client_enabled"=true,"pagination_items_per_page"=20},
 *     collectionOperations={"get"={"normalization_context"={"groups"={"listDiscipline"}},},
 *     "post"={"denormalization_context"={"groups"={"newDiscipline"}},"normalization_context"={"groups"={"idDiscipline"}},}
 * },
 *     itemOperations={"get"={"normalization_context"={"groups"={"detailDiscipline"}}},
 *     "put"={"denormalization_context"={"groups"={"updateDiscipline"}},"normalization_context"={"groups"={"detailDiscipline"}},},
 *     "delete"={"controller"=CustomDiscipline::class,"method"="DELETE"}
 *   },
 * )
 * @ApiFilter(BooleanFilter::class, properties={"enabled"})
 * @ORM\Table(name="discipline")
 * @ORM\Entity
 */
class Discipline
{
   /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"idDiscipline","detailDiscipline","listDiscipline"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     * @Groups({"detailDiscipline","listDiscipline","newDiscipline","updateDiscipline"})
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     * @Groups({"detailDiscipline","listDiscipline","newDiscipline","updateDiscipline"})
     * @Assert\NotBlank
     */
    private $description;

    /**
     * @var bool
     * @ORM\Column(name="enabled", type="boolean")
     * @Groups({"detailDiscipline","listDiscipline","newDiscipline","updateDiscipline"})
     * @Assert\NotBlank
     */
    private $enabled;

    /**
     *
     * @ORM\ManyToMany(targetEntity="EntitySport", mappedBy="disciplines")
     */
    private $entitySports;

     /**
     *
     * @ORM\ManyToMany(targetEntity="dependence", mappedBy="disciplines")
     */
    private $dependences;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Discipline
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Discipline
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Discipline
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->entitySports = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dependences = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add entitySports
     *
     * @param \App\Entity\EntitySports $entitySports
     * @return Discipline
     */
    public function addEntitySport(\App\Entity\EntitySport $entitySports)
    {
        $this->entitySports[] = $entitySports;
        return $this;
    }

    /**
     * Remove entitySports
     *
     * @param \App\Entity\EntitySports $entitySports
     */
    public function removeEntitySport(\App\Entity\EntitySport $entitySports)
    {
        $this->entitySports->removeElement($entitySports);
    }

    /**
     * Get entitySports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntitySports()
    {
        return $this->entitySports;
    }

    /**
     * Add dependences
     *
     * @param \App\Entity\dependence $dependences
     * @return Discipline
     */
    public function addDependency(\App\Entity\dependence $dependences)
    {
        $this->dependences[] = $dependences;
        return $this;
    }

    /**
     * Remove dependences
     *
     * @param \App\Entity\dependence $dependences
     */
    public function removeDependency(\App\Entity\dependence $dependences)
    {
        $this->dependences->removeElement($dependences);
    }

    /**
     * Get dependences
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getdependences()
    {
        return $this->dependences;
    }
}
