<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;

/**
 * State
 * @ApiResource(
 *     collectionOperations={"get"={"normalization_context"={"groups"={"listState"}},},
 *     "api_countries_states_get_subresource"={"method"="GET", "normalization_context"={"groups"={"listState"}},},
 *     "post"={"denormalization_context"={"groups"={"pState"}},"normalization_context"={"groups"={"idState"}},}
 * },
 *     itemOperations={"get"={"normalization_context"={"groups"={"detailState"}}},
 *     "put"={"denormalization_context"={"groups"={"pState"}},"normalization_context"={"groups"={"idState"}},},
 *     "delete"
 *   },
 *     )
 * @ApiFilter(BooleanFilter::class, properties={"enabled"})
 * @ORM\Table(name="state")
 * @ORM\Entity
 */
class State
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer",nullable=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"idState","listState","detailState"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255,nullable=true)
     * @Assert\NotBlank
     * @Groups({"detailState","pState","listState"})
     */
    private $name;

    /**
     * @var bool
     * @ORM\Column(name="enabled", type="boolean",nullable=true)
     * @Assert\NotNull
     * @Groups({"detailState","pState","listState"})
     */
    private $enabled;
     /**
     *
     * @ORM\OneToMany(targetEntity="City", mappedBy="state")}
     * @ApiSubresource
     */
    private $cities;

    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="states")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\NotNull
     * @Groups({"detailState","pState"})
     * @Groups({"detailCity","pCity"})
     */
    private $country;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return State
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return State
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cities = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add cities
     *
     * @param \App\Entity\City $cities
     * @return State
     */
    public function addCity(\App\Entity\City $cities)
    {
        $this->cities[] = $cities;

        return $this;
    }

    /**
     * Remove cities
     *
     * @param \App\Entity\City $cities
     */
    public function removeCity(\App\Entity\City $cities)
    {
        $this->cities->removeElement($cities);
    }

    /**
     * Get cities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * Set country
     *
     * @param \App\Entity\Country $country
     * @return State
     */
    public function setCountry(\App\Entity\Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \App\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }
}
