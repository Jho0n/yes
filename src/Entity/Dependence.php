<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\CustomDependence;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
/**
 * Dependence
 * @ApiResource(
 *     attributes={"pagination_enabled"=true,"pagination_client_enabled"=true,"pagination_items_per_page"=20},
 *     collectionOperations={"get"={"normalization_context"={"groups"={"listDependence"}},},
 *     "post"={"denormalization_context"={"groups"={"newDependence"}},"normalization_context"={"groups"={"idDependence"}},}
 * },
 *     itemOperations={"get"={"normalization_context"={"groups"={"detailDependence"}}},
 *     "put"={"denormalization_context"={"groups"={"updateDependence"}},"normalization_context"={"groups"={"idDependence"}},},
 *     "delete"={"controller"=CustomDependence::class,"method"="DELETE"}
 *   },
 * )
 * @ORM\Entity
 * @ApiFilter(BooleanFilter::class, properties={"enabled","reference"})
 */
class Dependence
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"idDependence","listDependence"})
     */
    protected $id;
    
     /**
     * @Groups({"newP"})
     * @ORM\Column( type="boolean", length=255)
     * @Groups({"newDependence"})
     * @Assert\NotBlank
     */
    protected $reference;
    
    
    /**
     * @Groups({"newP"})
     * @ORM\Column(name="name", type="string", length=255)
     * @Groups({"detailDependence","listDependence","newDependence","updateDependence"})
     * @Assert\NotBlank
     */
    protected $name;
    

    /**
     * @var bool
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     * @Groups({"detailDependence","listDependence","newDependence","updateDependence"})
     * @Assert\NotBlank
     */
    protected $enabled;

     /**
     * @ORM\ManyToMany(targetEntity="Discipline",inversedBy="dependences")
     * @Groups({"newDependence","updateDependence"})
     * @Assert\NotNull
     */
    protected $disciplines;

    /**
     * @ORM\ManyToOne(targetEntity="EntitySport",inversedBy="dependences")
     */
    protected $entitySport;

    public function __construct()
    {
        $this->disciplines = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Dependence
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
     /**
     * Set reference
     * @param string $reference
     * @return Dependence
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * Get reference
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return Dependence
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Add disciplines
     *
     * @param \App\Entity\Discipline $disciplines
     * @return Dependence
     */
    public function addDiscipline(\App\Entity\Discipline $disciplines)
    {
        $this->disciplines[] = $disciplines;
        return $this;
    }

    /**
     * Remove disciplines
     *
     * @param \App\Entity\Discipline $disciplines
     */
    public function removeDiscipline(\App\Entity\Discipline $disciplines)
    {
        $this->disciplines->removeElement($disciplines);
    }

    /**
     * Get disciplines
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDisciplines()
    {
        return $this->disciplines;
    }

    /**
     * Set entitySport
     *
     * @param \App\Entity\EntitySport $entitySport
     * @return Dependence
     */
    public function setEntitySport(\App\Entity\EntitySport $entitySport = null)
    {
        $this->entitySport = $entitySport;
        return $this;
    }

    /**
     * Get entitySport
     *
     * @return \App\Entity\EntitySport
     */
    public function getEntitySport()
    {
        return $this->entitySport;
    }
}
